import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-user-coding',
  templateUrl: './test-user-coding.component.html',
  styleUrls: ['./test-user-coding.component.css']
})
export class TestUserCodingComponent implements OnInit {

  message;
 message2;
  address = {
    district: 'agadir',
    state: 'morocco'
  }

  ngOnInit() {
  }

function(){
    type ColorType = [string, number, number, number];
let red: ColorType = ['Red', 1, 0, 0];
let green: [string, number, number, number] = ['Green', 0, 1, 0];
let blue = ['Blue', 0, 0, 1]; 
  this.message = blue;
}

function2(){

let stringType: string = "string type";
stringType.toUpperCase();

let anyType: any = "any type";
anyType.toUpper();
anyType.toUpperCase();

let objectType: Object = "object type";
objectType.toString().toUpperCase();
}
 
  setMsg(data: string) {
    console.log("event : " )
    this.message = data;
  }

  // setMsg(event) { marche aussi 
  //   console.log("event : ", event );
  //   this.message = event;
  // }
  constructor() {

  }

}
