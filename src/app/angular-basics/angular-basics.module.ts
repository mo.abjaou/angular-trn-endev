import { AppComponent } from "../app.component";
import { AngularBasicsComponent } from "./angular-basics.component";
import { ComposantsComponent } from "./composants/composants.component";
import { ListEmployeComponent } from "./composants/list-employe/list-employe.component";
import { EmployeComponent } from "./composants/list-employe/employe/employe.component";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "../app-routing.module";
import { FormsModule } from "@angular/forms";
import { EmployeManagerService } from "./composants/employe-manager.service";
import { MultiplicationPipe } from "./composants/multiplication.pipe";
import { PuissancePowerPipe } from "./composants/puissance.pipe";
import { employeHeureuxPipe } from "./composants/employeHereux.pipe";
import { employeHeureuxImpurePipe } from "./composants/employeHeureuxImpure.pipe";
import { ColorDirective } from "./composants/color.directive";
import { HttpClientModule } from "@angular/common/http";
import { TestAvrilComponent } from './composants/test-avril/test-avril.component';
import { TestUserCodingComponent } from './test-user-coding/test-user-coding.component';
 
@NgModule({
    declarations: [
      AngularBasicsComponent,
      ComposantsComponent,
      ListEmployeComponent,
      EmployeComponent,
      MultiplicationPipe,
      PuissancePowerPipe,
      employeHeureuxPipe,
      employeHeureuxImpurePipe,
      ColorDirective,
      TestAvrilComponent,
      TestUserCodingComponent
    ],
    
    imports: [//importer les externe 
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
    ],
    
    providers: [EmployeManagerService],//les services pour injecter un service dans un services
    bootstrap: [AppComponent]
  })
  export class AppAgularBasicsModule { }