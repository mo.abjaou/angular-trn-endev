import { Component, OnInit, Input, EventEmitter, Output, Directive } from '@angular/core';
import { Employe } from '../../employe';


 
@Component({
  selector: 'app-employe',
  templateUrl: './employe.component.html',
  styleUrls: ['./employe.component.css']
})
export class EmployeComponent implements OnInit {
   
  color = 'blue';
  public identifiant:string;
  @Input() employeToBind: Employe;//Employe est le composant pere de 2eme niveau
  //EventEmitter dyal @angular/core
  @Output() public estPresent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public estAbsent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() set id(id:number){
    //bb:number=id+1;
    this.identifiant= 'identifiant :'+id;
  }

  disactiverButton = '';
  //@Input() id:number;
  
  constructor() { }

  ngOnInit() {
  }

  marquerPresent(){
    this.estPresent.emit(true);
    this.disactiverButton ='disabled';
  }

  marquerAbsent(){
    this.estAbsent.emit(false);
    this.disactiverButton ='disabled';
  }


}
