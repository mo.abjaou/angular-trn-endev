import { Component, OnInit, Input } from '@angular/core';
import { Employe } from '../employe';

@Component({
  selector: 'app-list-employe',
  templateUrl: './list-employe.component.html',
  styleUrls: ['./list-employe.component.css']
})
export class ListEmployeComponent implements OnInit {
//vrai listeemployesToBind est la meme que listeEmployes donc [listeEmployes] = [listeemployesToBind]
//Employe est le composant pere
  @Input('listeemployesToBind') listeEmployes:Employe[];

  public listePresents: Employe[] = [];
  public listeAbsents: Employe[] = [];
  
  constructor() { }

  ngOnInit() {
    console.log(this.listeEmployes);
  }

  listeDesPresents(employe: Employe){
    this.listePresents.push(employe);
    console.log(employe);
  }

  listeDesAbsents(employe: Employe){
    this.listeAbsents.push(employe);
    console.log(employe);
  }



}
