import { Component, OnInit } from '@angular/core';
import { Employe, EMPLOYES } from './employe';
import { EmployeManagerService } from './employe-manager.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-composants',
  templateUrl: './composants.component.html',
  styleUrls: ['./composants.component.css']
})
export class ComposantsComponent implements OnInit {

  public title1 = '1- Affichage de données (Interpolation + Directives structurelle)';
  public title2 = '2- Directive Attribue';
  public title3 = '3- Liaison de données entre la Template et le Controleur';
  public title4 = '4- Partage de donnée entre deux composant (Input & Output)';
  public title5 = '5-Pile';

  nom:string ='mohamed';

  myFirstName:string ="zaakaria";

  // deb pipe variables
  myPuissanceNumber = 10;

  puissanceDe = 1 ;

  employes:Employe[] = [];
  emotion = true;



  //variable pour les directives
  color = 'red';

  idemploye;

  info;

  ajouterEmploye(nom:string){
    if(!nom){
      return;
    }
  

  const emotion = (this.emotion) ? 'heureux' : 'triste';
  const prenom = nom;
  const age=20;
  const id=77;
  console.log('émotion: '+emotion);
  const employe = {id,nom , prenom , age , emotion};
  this.employes.push(employe);

  }

  reinitialiser(){
    this.employes = [];
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  //end pipe variable
  

  //récuperation de la liste des employé ap partr d'une constante
  //public listeEmploye:Employe[] = EMPLOYES;

  public listeEmploye:Employe[] ;

  //variable pour les PILES
  todayDate : Date = new Date();

  constructor(private emplyeManagerService: EmployeManagerService) {


   }

  onAction(){
    if(!this.nom.includes('mon prénom est : ')){
      this.nom="mon prénom est : "+this.nom;
    }

    
  }

  ngOnInit() {
    //récuperation de la liste des emplayé a partir d'un service
    this.listeEmploye = this.emplyeManagerService.getEmployes();

    //from json
    
    //récupérer la liste des employer apar d'un serveur
    this.getEmployesServeur();
  
  }

  getColor(age:number){
    if(age<=20){
      return 'text-success';
    } else if (age <= 40){
      return 'text-primary';
    } else if (age <= 60){
      return 'text-warning';
    } else if (age <= 80){
      return 'text-danger'; 
    }

  }

  private getEmployesServeur(): void {
    this.emplyeManagerService.getEmployees().subscribe(
      (listeEmployees: Employe[]) => {
        console.log('list emloyees :', listeEmployees);
        this.employes = listeEmployees;
      },

      (error) => {
        console.error('Erreur personnalitée');
        console.error(error);
      },

      () => {
        console.log('c\' est fini');
      }
  
    );

  }

  public addEmploye(){
    const employe1: Employe = new Employe('toto','tata',40,'HH');
    this.emplyeManagerService.addEmployees(employe1).subscribe((emp:Employe) => {
      if(isNullOrUndefined(emp)){
        console.log();
      }
    },
    (error) => {

    }
    )
    //this.emplyeManagerService.addEmployees(employe1);
    //this.employ
  }

  addEmp(){
    const emp = new Employe('kkk', 'fff', 21,'');
    this.emplyeManagerService.addEmployees(emp).subscribe();
  }

  public deleteEmp(){
    const id = this.idemploye;
    this.emplyeManagerService.deleteEmp(id).subscribe((data) =>{
      console.log('employé supprimé');
      this.getEmployesServeur();
    },
    (error) =>{
      console.error(error);
    }
    );

}
/* public modifEmp(){
  const id = this.idemploye;
  const empl = new Employe()
  return this.http.delete<any>('http://localhost:3000/employees/'+id);
} */
}
