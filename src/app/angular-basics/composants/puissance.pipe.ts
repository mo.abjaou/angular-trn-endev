import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name :'multiplicatPipe'})
export class PuissancePowerPipe implements PipeTransform{

    

    transform(numbre:any, puissance:string){
        
        // if(puissance == null && puissance == ''){
        //     puissance = '1';
        // }

        // const val =  (isNaN(puissance) ? 1 : parseFloat(puissance));
        
        // return Math.pow(numbre,val);//les ternaires

        const val = parseFloat(puissance);
        
        return Math.pow(numbre,val);
    }
}