import { Employe } from "./employe";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class EmployeManagerService {
    private listEmploye: Employe[];
    private urlAssets = 'assets/employees.json';

    constructor(private http: HttpClient){

    }

    public getEmployes(): Employe[]{
        this.listEmploye=[];
        const employe1: Employe = new Employe('totoNom','totoPrenom',20,'triste');
        const employe2: Employe = new Employe('totoNom1','totoPrenom1',10,'heureux');
        const employe3: Employe = new Employe('totoNom2','totoPrenom2',30,'triste');
        const employe4: Employe = new Employe('totoNom3','totoPrenom3',60,'heureux');
        const employe5: Employe = new Employe('totoNom4','totoPrenom4',80,'triste');
        const employe6: Employe = new Employe('totoNom5','totoPrenom5',25,'heureux');

        this.listEmploye.push(employe1);
        this.listEmploye.push(employe2);
        this.listEmploye.push(employe3);
        this.listEmploye.push(employe4);
        this.listEmploye.push(employe5);
        this.listEmploye.push(employe6);
        return this.listEmploye;

    }

    public getEmployees(): Observable<Employe[]>{
       return this.http.get<Employe[]>("http://localhost:3000/employees");
        //return this.http.get<Employe[]>(this.urlAssets);  
    }

    // public addEmployees(): Observable<Employe[]>{
          
    //  }
    public addEmployees(employee: Employe): Observable<Employe>{
        return this.http.post<Employe>('http://localhost:3000/employees', employee);
    }

    public deleteEmp(id: number): Observable<any>{
        return this.http.delete<any>('http://localhost:3000/employees/'+id);
    }

    public modifEmp(id: number,empl:Employe): Observable<any>{
        return this.http.put<any>('http://localhost:3000/employees/'+id, empl);
    }
  
}