import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name :'multiplicatPipe'})
export class MultiplicationPipe implements PipeTransform{
    //PipeTransform pour dire au syst comme quoi c une pipe

    transform(value:any, multiple:string){
        const val = parseFloat(multiple);
        return value * (isNaN(val) ? 1 : val);//les ternaires
    }


}