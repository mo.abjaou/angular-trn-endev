import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-avril',
  templateUrl: './test-avril.component.html',
  styleUrls: ['./test-avril.component.css']
})
export class TestAvrilComponent implements OnInit {
  codingGame : "iwill";
  user = {
    name : "abjaou mohamed",
    email : "mo.abbjaou@gmail.com",
    age : "27"
  }
  constructor() { }

  ngOnInit() {
  }

}
