import { Directive, ElementRef, Renderer2, Input, HostListener } from "@angular/core";

//on va faire une directive attribue donc []
@Directive({ selector:'[appColor]'})
export class ColorDirective {


    @Input() appColor;
    i=0;

    constructor(private el: ElementRef, private renderer: Renderer2){
        //private droit d'acces
        //Renderer2 permet d applq un style
        console.log('couleur',this.appColor);
        // renderer.setStyle(el.nativeElement, 'color', 'red');
        // renderer.setStyle(el.nativeElement, 'border', '2px solid black');
    }

    ngOnInit(){
        //injection de dependance
        console.log('couleur',this.appColor);
        console.log(" renderer ", this.renderer);
        console.log(" this.renderer.setStyle(this.el", this.el);
        console.log(" this.renderer.setStyle(this.el.nativeElement ", this.el.nativeElement);
        this.renderer.setStyle(this.el.nativeElement, 'color', 'green');
        this.renderer.setStyle(this.el.nativeElement, 'border', '2px solid black');

    }

    
    @HostListener('mouseenter',['$event']) onMouseEnter(event: Event){
        this.i++;
        //console.log('mouse enter => iteration',this.i);
        console.log('mouse enter');
        this.renderer.setStyle(this.el.nativeElement, 'color', this.appColor);
        console.log("this.el.nativeElement ",this.el.nativeElement);
        console.log("this.el ",this.el);

        console.log(event);
    }

    @HostListener('mouseleave',['$event']) onMouseLeave(event: Event){
        this.i++;
        console.log('mouse leave');
        this.renderer.setStyle(this.el.nativeElement, 'color', 'blue');
        console.log('mouse enter => iteration',this.i);
        console.log(event);
        
    }


}