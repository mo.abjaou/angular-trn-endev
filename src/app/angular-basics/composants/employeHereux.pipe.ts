import { PipeTransform, Pipe } from "@angular/core";
import { Employe } from "./employe";


@Pipe({name :'employeHeureux'})
export class employeHeureuxPipe implements PipeTransform{
    //PipeTransform pour dire au syst comme quoi c une pipe

    transform(employes: Employe[]){
         
        return employes.filter(employe => employe.emotion === 'heureux');//=== verifie aussi le type
    }


}