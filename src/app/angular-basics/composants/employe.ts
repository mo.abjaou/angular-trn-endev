export class Employe{
    id;
    constructor(public nom:string, public prenom:string, public age:number, public emotion:string){
    }
}

export const EMPLOYES = [
    new Employe('toto','toto',20,'sad'),
    new Employe('khalid','toto',45,'sad'),
    new Employe('kamal',"abakd",18,'happy'),
    new Employe('rachid','toto',0,'happy'),
    new Employe('hassan','toto',17,'happy'),
    new Employe('yassine','toto',40,'sad'),
    new Employe('mehdi','toto',60,'no emotion'),
    new Employe('kiki','toto',75,'no emotion'),
];