import { NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { AngularBasicsComponent } from './angular-basics/angular-basics.component';
import { Erreur404Component } from './core/erreur404/erreur404.component';
// import { AngularBasicsComponent} from './';





const appRoutes: Routes = [
    {path:'',redirectTo:'/angular-basics', pathMatch:'full'},
    {path:'angular-basics', component: AngularBasicsComponent},
    {path:'**', component: Erreur404Component},
];

@NgModule({
    imports :[
        RouterModule.forRoot(
            appRoutes
        )
        
    ],
    exports:[RouterModule]
})

export class AppRoutingModule{

}

    



