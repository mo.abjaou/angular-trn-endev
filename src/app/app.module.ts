import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { Erreur404Component } from './core/erreur404/erreur404.component';
import { AppRoutingModule } from './app-routing.module';
import { AppAgularBasicsModule } from './angular-basics/angular-basics.module';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    Erreur404Component
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AppAgularBasicsModule
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
